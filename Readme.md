# Oracle Cloud - Ubuntu 18.04 Basic Setup

## Introduction

I have some Oracle Virtual Private Servers (Instances). I have been using them for educational purposes. I'm resetting them occationally and I have to do some basic things over a over again everytime when I reset the vps.

So, I have decided to write this damn simple script to do that basic setup automatically.

## Usage

1. Login to your Ubuntu 18.04 vps with SSH (I have only testsed this for Ubuntu 18.04)
2. Run this command and sit tight!

`curl -sSL https://gitlab.com/optimuspnj/oracle-cloud-vps-ubuntu-18.04-basic-setup-script/-/raw/dev/script.sh | bash`